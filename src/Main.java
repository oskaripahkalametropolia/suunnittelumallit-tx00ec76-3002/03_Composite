import model.*;
import implemantation.*;

public class Main {
    public static void main(String[] args) {
        Component processor = new CPU("AMD Ryzen 7 5700G", 369.90);
        Component motherboard = new Motherboard("Asus TUF Gaming X570-Plus", 219.90);
        Component cooler = new Cooler("Noctua NH-U12S-SE-AM4", 79.90);
        Component memory = new RAM("Kingston FURY Beast DDR4", 150.90);
        Component graphics = new GPU("Asus GeForce TUF-RTX3080TI-O12G-GAMING", 1869.90);
        Component ssd = new Storage("Samsung 870 EVO SSD 1 Tt", 129.90);
        Component computer = new Case("Fractal Design Meshify 2 ATX", 159.90);

        motherboard.addComponent(processor);
        motherboard.addComponent(memory);
        motherboard.addComponent(cooler);
        computer.addComponent(motherboard);
        motherboard.addComponent(graphics);
        computer.addComponent(ssd);

        System.out.println(computer);
        System.out.printf("Total: %.2f€%n", computer.getTotalPrice());
    }
}
