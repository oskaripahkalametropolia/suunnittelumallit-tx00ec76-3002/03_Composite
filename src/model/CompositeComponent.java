package model;

import java.util.ArrayList;

public abstract class CompositeComponent extends SimpleComponent {
    private final ArrayList<Component> subcomponents;

    public CompositeComponent(String name, double price) {
        super(name, price);
        subcomponents = new ArrayList<>();
    }

    public void addComponent(Component component){
        subcomponents.add(component);
    }

    public double getTotalPrice(){
        return super.getPrice() + subcomponents.stream()
                .mapToDouble(Component::getTotalPrice)
                .sum();
    }

    @Override
    public String toString(){
        return "%s%n%s".formatted(
                subcomponents.stream()
                        .map(Component::toString)
                        .reduce("", "%s%n%s"::formatted),
                super.toString()
        );
    }
}
