package model;

public interface Component {
    String getName();
    double getPrice();
    double getTotalPrice();
    void addComponent(Component component);
}
