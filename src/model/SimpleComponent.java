package model;

public abstract class SimpleComponent implements Component {
    private final String name;
    private final double price;

    public SimpleComponent(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public void addComponent(Component component) {
        throw new RuntimeException("Cannot add components to simple components.");
    }

    public String getName() {
        return this.name;
    }

    public double getPrice() {
        return this.price;
    }

    public double getTotalPrice() {
        return getPrice();
    }

    @Override
    public String toString() {
        return "%s: %.2f€".formatted(name, price);
    }
}
